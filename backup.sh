#!/usr/bin/env bash

snap_date=$(date +%m-%d-%Y)

read -p "Enter the directory you want to back up: " target 
read -p "Enter the directory you want to store the backup in: " dest

tar -czpvf - $target | gpg -c --cipher-algo aes256 -o $dest/$snap_date.tar.gz.gpg
