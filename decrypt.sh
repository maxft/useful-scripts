#!/usr/bin/env bash
##
# Decrypts encrypted tar archives made by backup.sh
##

gpgtar -d $1 | tar xzvf -
