#!/usr/bin/env bash

if [ "$(id -u)" -ne 0 ]; then
	# Print error message
	echo "This must be run as root. Do 'sudo !!' to try again." >&2
	exit 1 # Exit with error status
fi

read -p "Enter the full path to the iso image you want: " iso
read -p "Enter the device name you want to flash the image to: " device

mkfs.vfat -F32 "$device" -I
dd bs=4M if="$iso" | pv -tpre | dd of="$device" && sync
