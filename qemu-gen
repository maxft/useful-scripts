#!/usr/bin/env bash

# Usage: qemu-gen name-of-vm

# This function sets up a bridge interface
bridge() {
    echo 'allow br0' | sudo tee -a /etc/qemu/bridge.conf
    sudo ip link add name br0 type bridge
    sudo ip link set br0 up
    sudo ip link set eth0 master br0
}

echo -e "Welcome to QEMU VM setup!\n"

# Writing the beginning of the launch script we're generating
echo -e '#!/usr/bin/env bash\n\nOPTS=""' >> $1.sh

# Asking for core count
read -p "How many cores do you want to VM to have?: " cores
echo -e "\n# CPU\nOPTS=\"\$OPTS -cpu host -enable-kvm -smp $cores\"" >> $1.sh # We're appending parameters to this shell script we're making

# Asking for amount of memory
read -p "How much RAM in Gigabytes?: " ram
echo -e "# RAM\nOPTS=\"\$OPTS -m ${ram}G\"" >> $1.sh

# Switch statement for networking
echo 'What networking setup do you want?: '
select pick in "User" "SSH" "Bridged"; do
    case $pick in
        User)
            echo 'Setting up Virtio NIC in User mode' # User networking is the default, so we do nothing here
            echo -e "# Networking\nOPTS=\"\$OPTS -nic user,model=virtio-net-pci\""
            break;;
        SSH)
            echo 'Setting up SSH at port 2222...'
            echo -e "# Networking\nOPTS=\"\$OPTS -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2222-:22\"" >> $1.sh # Creating virtual network and binding host tcp 2222 to 22
            break;;
        Bridged)
            echo 'Creating new bridge br0...' 
            bridge # We're calling the bridge function we instantiated earlier
            break;;
        *)
            echo 'Invalid choice, exiting...'
            exit 1 # Bad inputs will cause the script to halt
    esac
done

read -p "Enter the path to the installation ISO: " iso
read -p "What disk format do you want (enter qcow or raw)?: " fmt
read -p "Enter disk size in Gigabytes: " size
qemu-img create -f $fmt $1.$fmt ${size}G
echo -e "# Disks\nOPTS=\"\$OPTS -cdrom $iso -drive file=$1.$fmt,if=virtio\"" >> $1.sh

echo 'What graphics options will you choose?: '
select grp in "std" "spice" "virtio" "none"; do
    case $grp in
        std)
            echo 'Using std...' # std is the default, so we don't have to do anything
            break;;
        spice)
            echo 'Using spice...'
            echo -e "# Graphics\nOPTS=\"\$OPTS -vga qxl -global qxl-vga.vram_size_mb=256\"\nOPTS=\"\$OPTS -display sdl\"\n# Spice\nOPTS=\"\$OPTS -spice unix=on,addr=/tmp/vm_spice.socket,disable-ticketing=on\"" >> $1.sh
            break;;
        virtio)
            echo 'Using virtio'
            echo -e "# Graphics\nOPTS=\"\$OPTS -vga virtio -display sdl,gl=on\"" >> $1.sh
            break;;
        none)
            echo 'Disabling graphics'
            echo -e "# Graphics\nOPTS=\"\$OPTS -nographic\"" >> $1.sh
            break;;
        *)
            echo 'Invalid choice, exiting...'
            exit 1
    esac
done

read -p 'Audio? (y,n): ' audio
if [[ "$audio" == y ]]; then
    echo 'Adding audio support...'
    # Emulating a Intel sound card
    echo -e "# Audio\nOPTS=\"\$OPTS -device ich9-intel-hda,addr=0x1b\"\nOPTS=\"\$OPTS -device hda-micro,audiodev=hda\"\nOPTS=\"\$OPTS -audiodev pa,id=hda,server=unix:/run/user/1000/pulse/native\"" >> $1.sh
elif [[ "$audio" == n ]]; then
    echo 'Proceeding without audio support...'
else
    echo 'Invalid choice, exiting...'
    exit 1
fi

read -p 'Using Windows? (y,n): ' win
if [[ "$win" == y ]]; then
    echo -e "# Fix clock\nOPTS=\"\$OPTS -rtc clock=host,base=localtime\"\n# Mouse\nOPTS=\"\$OPTS -usb -device usb-tablet\"" >> $1.sh # We're resolving time conflicts & fixing mouse issues
elif [[ "$win" == n ]]; then
    echo 'Proceeding...'
else
    echo 'Invalid choice, exiting...'
    exit 1
fi

echo -e "\n# Run\nqemu-system-x86_64 \$OPTS" >> $1.sh # Writing the final line for the script we're making that runs all of the parameters we've set
echo 'Finished setup. Have a nice day!'
