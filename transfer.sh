#!/bin/bash

read -p "How many allowed downloads?: " downs
read -p "Enter a file deletion period between 1 and 14 days: " del

curl -H "Max-Downloads: \"$downs\"" -H "Max-Days: \"$del\"" --upload-file "$1" https://transfer.sh/
